<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Trip;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class DefaultController extends Controller {

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request) {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig');
    }

    /**
     * @Route("/home", name="nowa")
     */
    public function nowaAction(Request $request) {
        return $this->render('/home.html.twig');
    }

    /**
     * @Route("/top100", name="top100")
     */
    public function tio100Action(Request $request) {
        if ($request->files->get('imageFile')) {

            $up = new \AppBundle\Entity\Uploads();

            $up->setImageFile($request->files->get('imageFile'));
            $up->setImageName($request->get('imageName'));
            $up->setImageSize(filesize($request->files->get('imageFile')));

            $em = $this->getDoctrine()->getManager();
            $em->persist($up);
            $em->flush();
        }


        return $this->render('/top100.html.twig');
    }

    /**
     * @Route("/dom", name="dom")
     */
    public function domAction(Request $request) 
   {
       $entityManager = $this->getDoctrine()->getManager();
       $img = $entityManager->getRepository(\AppBundle\Entity\Uploads::class)->findBy([], ['updatedAt' => 'desc']);
       
  
       
       return $this->render('/dom.html.twig', ['img' => $img, 'images_dir' => '/img/upload']);
   }
    /**
     * @Route("/trip", name="trip")
     */
    public function tripAction(Request $request) {
        
        $user = $this->getUser();
//          
        $trip = new Trip();
//        $trip->setCity('Miasto do którego chcesz pojechać');
//        $trip->setCommend('tutaj możesz napisac jakis komentarz');
//        $trip->setRating('tutaj możesz ocenić wycieczkę');

        $form = $this->createFormBuilder($trip)
            ->add('city', TextType::class)
            ->add('commend', TextType::class, array('required'=> false))
            ->add('rating', TextType::class)    
            ->add('save', SubmitType::class, array('label' => 'Wyslij!?'))
            ->getForm();
        
           $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
        // $form->getData() holds the submitted values
        // but, the original `$task` variable has also been updated
        $trip = $form->getData();

         $trip->setUser($user);
         $em = $this->getDoctrine()->getManager();
         $em->persist($trip);
         $em->flush();

        return $this->redirectToRoute('nowa');
    }

        return $this->render('trip.html.twig', array(
            'form' => $form->createView(),
        ));
    }
    
     /**
     * @Route("/wyprawy", name="wyprawy")
     */
    public function wyprawyAction(Request $request) {
        
        $em = $this->getDoctrine()->getManager();
        $wyprawy = $em->getRepository(Trip::class)->findAll();
        
        
        return $this->render('/wyprawy.html.twig', ["wyprawy" => $wyprawy]);
    }
    
    /**
     * @Route("/under", name="UnerCinstaction")
     */
    public function underAction(Request $request) {
        return $this->render('/under.html.twig');
    }
   
}
